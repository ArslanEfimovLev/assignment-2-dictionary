global exit
global string_length
global print_string
global print_error
global print_char
global print_newline
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy



%define SYS_IN 0
%define SYS_OUT 1
%define STD_ERR 2
%define WRITE_SYS_NUMBER 1
%define READ_SYS_NUMBER 0
%define SYS_EXIT 60
%define STR_END 0
%define TRUE 1
%define FALSE 0
%define BASE_DIVIDER 10

section .text


; Принимает код возврата и завершает текущий процесс
exit:

mov rax, SYS_EXIT
syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину

string_length:
        xor rax, rax

        .str_length_count:
                cmp byte [rdi+rax], STR_END ;если 0 терминатор, то конец иначе увеличиваем счетчик
                je .end
                inc rax
                jmp .str_length_count

        .end:
                ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout

        print_string:
                push rdi ; Сохраняем rdi
                call string_length ; считаем длину строки
                pop rsi  ; адрес начала строки передаем в rsi
                mov rdx, rax ; в rdx копируем значение длины строки
                mov rax, WRITE_SYS_NUMBER ; stdout rax for syscall
                mov rdi, SYS_OUT ; stdout for syscall
                syscall
                ret

; Выводит поток ошибок в stderr, указатель на 0-терминированную строку
print_error:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, WRITE_SYS_NUMBER
    mov rdi, STD_ERR
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
        push rdi ; кладем адрес начала символа в стек
        mov rsi, rsp ; передаем значение указателя стека в rsi
        mov rdx, 1
        mov rax, WRITE_SYS_NUMBER
        mov rdi, SYS_OUT
        syscall
        pop rdi ; восстанавливаем rdi
        ret



; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
    jmp print_char


; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test rdi, rdi ; проверка на знаковое число
    js .signed
    jmp print_uint
    .signed:
        push rdi ; сохраняем rdi
        mov rdi, '-' ;
        call print_char ; выводим минус
        pop rdi ; загружаем rdi
        neg rdi ; преобразование в прямой код

; Выводит беззнаковое 8-байтовое число в десятичном формат
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
        xor rax, rax
        mov rax, rdi
        mov rsi, rsp
        push 0 ; выделяем место в стеке
        mov r9, BASE_DIVIDER ; записываем в r9 делитель

        .devide:
        xor rdx, rdx ; обнуляем остаток
        div r9 ; делим rax на 10, целое в rax, остаток в rdx
        add dl, '0' ; преобразуем в ASCII код
        dec rsp ; сдвигаем буфер влево
        mov byte[rsp], dl ; остаток в rsp
        test rax, rax ; если не 0, то делим еще
        jnz .devide

        mov rdi, rsp ; передаем указатель в rdi
        push rsi ; сохраняем rsi
        call print_string
        pop rsi ; загружаем rsi
        mov rsp, rsi ; восстанавливаем rsp
        ret



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .while:
        mov bpl, [rdi] ; записываем символ в bpl
        cmp bpl, [rsi] ; сравниваем с символом другой строки
        jz .equals
        jmp .not_equals

        .equals:
        inc rdi ; сдвигаем указатель первой строки
        inc rsi ; сдвигаем указатель второй строки
        cmp bpl, STR_END ; проверка на конец строки
        jnz .while ; если не конец, то продолжаем цикл
        mov rax, TRUE ; иначе возвращаем 1
        ret

        .not_equals:
        mov rax, FALSE ; возвращаем 0, так как не равны
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, READ_SYS_NUMBER ; mov syscall number
    mov rdi, SYS_IN 
    push rax 
    mov rsi, rsp 
    mov rdx, 1
    syscall
    pop rax
    ret 
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    ; Сохраняем регистры
    	push r12
	push r13
	push r14
	xor r13, r13
	mov r14, rdi
	mov r12, rsi
	.delete_space_symbols:
		call read_char
		cmp rax, ' '	
		je .space
		cmp rax, `\t`
		je .space
		cmp rax, `\n`
		je .space
	.loop:	
		cmp rax, `\n`
		jz .end
		mov [r14 + r13], al
		cmp r13, r12
		jz .not_correct
		inc r13
		jmp .delete_space_symbols
	.space:
		test r13, r13
		je .delete_space_symbols
		jmp .loop
	.not_correct: 	
        xor rax, rax
		jmp .ret
	.end: 	
		mov byte[r14 + r13], STR_END
		mov rax, r14
		mov rdx, r13
	.ret:	
		pop r14
		pop r13
		pop r12
		ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rcx, rcx
    xor rdx, rdx
    xor rax, rax

        .loop:
                mov cl, byte[rdi+rdx] ; копируем символ в младшие биты rcx
                test cl, cl ; проверяем на конец строки
                jz .end

                cmp cl,'0' ; если меньше 0, то прыгаем в n_digit
                jl .n_digit

                cmp cl, '9' ; если больше 9, то прыгаем в n_digit
                jg .n_digit

                sub rcx, '0' ; переводим ASCII в число
                imul rax, rax, 10 ; умножаем rax на 10 и записываем в rax
                add rax, rcx ; добавляем символ
                inc rdx ; увеличиваем длину
                jmp .loop

        .n_digit:
                cmp rax, 0
                jnz .end
                xor rdx, rdx
        .end:
                ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor r11, r11
    mov r11b, byte[rdi] ; копируем символ в r11b
    cmp r11b, '-' ; если знаковое, то переходим в neg_digit
    je .neg_digit

    call parse_uint ; записываем в rax число
    jmp .end



    .neg_digit:
        inc rdi ; сдвигаем указатель
        call parse_uint ; записываем в rax число
        inc rdx ; увеличиваем длину на знак
        neg rax ; переводим в прямой код

    .end:
        ret




; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0

string_copy:

    ; Сохраняем значение регистров
    push rsi
    push rdi
    push rdx
    call string_length ; считаем длину строки и записываем в rax
    pop rdx
    pop rdi
    pop rsi
    cmp rax, rdx ; если длина строки больше допустимого значения буфера, то прыгаем в more_than_buff
    jg .more_than_buff

    xor r9, r9 ; очищаем счетчик длины строки
    .while:
        mov r11b, byte[rdi + r9] ; записываем символ в r11b
        cmp r11b, STR_END ; сравниваем его с концом строки
        je .end

        mov byte[rsi+r9], r11b ; копируем символ в буфер
        inc r9 ; увеличиваем длину строки
        jmp .while

    .more_than_buff:
        xor rax, rax
        ret


        .end:
                mov byte[rsi+r9], 0 ; записываем 0 терминатор в конец строки буфера
                ret

    xor rax, rax
    ret
