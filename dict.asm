%include "lib.inc"
global find_word

section .text
find_word:

	; rdi - Указатель на нуль-терминированную строку
	; rsi - Указатель на начало словаря
	; rax - адрес на начало вхождения в словарь
	
	push r12
	push r13
	mov r12, rdi
	mov r13, rsi
	
	.loop:
	    test r13, r13
	    je .end
            mov rdi, r12
	    lea rsi, [r13+8] ; адрес начала поля ключа
            call string_equals
            test rax, rax
	    jne .end
            mov r13, [r13] ; переход к следующему элементу
	    jmp .loop
            
	.end:
	    mov rax, r13
	    pop r13
	    pop r12
	    ret
