%define last_label 0

%macro colon 2
 %ifid %2
   %ifstr %1
       %2:
           dq last_label 
           db %1, 0 
           %define last_label %2
   %else
     %error "Ключ не является строкой"
     %endif
 %else 
   %error "второй агрумент не является меткой"
   %endif 
%endmacro   
