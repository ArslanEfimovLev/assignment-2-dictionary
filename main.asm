%include "words.inc"
%include "lib.inc"
%include "dict.inc"

%define BUFFER_SIZE 255
section .bss
buffer resb 256

section .rodata
buffer_overflow_msg: db "The key more than buffer length", 0

not_fount_str: db "The key is not found", 0

section .text
global _start


_start:
	mov rdi, buffer
        mov rsi, BUFFER_SIZE
        call read_word
        test rax, rax ; если переполение буфера, то прыгаем на метку для печати ошибки об этом
        jz .buffer_overflow_error
        mov rdi, rax
        mov rsi, last_label ; указатель на начало вхождения в словарь (last_label you can see in colon.inc)
        call find_word ; ищем совпадение в словаре
        test rax, rax
        jz .value_not_found ; если не найдено, то прыгаем на метку для печати ошибки об этом
        mov rdi, rax
        add rdi, 8 ; получим адрес начала ключа
        push rdi
        call string_length ; найдем длину ключа
        pop rdi
        lea rdi, [rdi+rax+1] ; адрес начала значения в rdi +1 - 0-терминированная строка
        call print_string ; выводим значение
        call print_newline ; переходим на следующую строку
        xor rdi, rdi
        jmp .end

        .value_not_found:
                mov rdi, not_fount_str
                jmp .print_error

        .buffer_overflow_error:
                mov rdi, buffer_overflow_msg


        .print_error:
                call print_error
                call print_newline
		mov rdi, 1 ; код возрата при ошибке
        .end:
                call exit

