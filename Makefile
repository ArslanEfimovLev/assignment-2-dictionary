ASM = nasm
EFLAGS = -f elf64 -g

PYTHON = python

.PHONY: clean all test

all: main

clean:
	$(RM) main *.o

test:
	$(PYTHON) test.py

%.o:%.asm
	$(ASM) $(EFLAGS) -o $@ $<

main.o:main.asm lib.inc words.inc dict.inc
	$(ASM) $(EFLAGS) -o $@ $<

main:main.o dict.o lib.o
	ld -o $@ $^
